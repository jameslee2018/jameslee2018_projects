package testWebService_Client;

import java.util.Scanner;

import testWebService_Client.service.HelloService;
import testWebService_Client.service.HelloServiceService;
import org.json.JSONObject;

public class Hello {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("input name:");
		String name = sc.nextLine();			
			
		HelloService hello = new HelloServiceService().getHelloServicePort();
		String result = hello.sayHello(name);

		System.out.println("return json string   =      " + result);
		JSONObject jsString = new JSONObject(result);
		
		String helloString = jsString.getString("message");
		
		System.out.println("return hello string  =     " + helloString);
	}

}
