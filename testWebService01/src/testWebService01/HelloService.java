package testWebService01;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.xml.ws.Endpoint;

import org.json.JSONStringer;

@WebService
public class HelloService {
	
	@WebMethod
	public String sayHello(String name) {
		JSONStringer jsString = new JSONStringer();
		jsString.object();
		jsString.key("message");
	    jsString.value("Hello " + name);
		jsString.endObject();
		
		String strReturn = jsString.toString();
		return strReturn;
	}

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Endpoint.publish("http://localhost:9091/testWebService01/HelloService", new HelloService());
		System.out.println("success");
	}
	
}